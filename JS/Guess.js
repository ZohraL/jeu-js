class Guess {

    /**
     * 
     * @param {string} words 
     * @param {string} hints 
     * @param {string} images 
     */

    constructor(words, hints, images) {

        this.words = words;
        this.hints = hints;
        this.images = images;
    }
}