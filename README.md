Voici le 2ème projet de l'année. Le but est de créer un jeu en utilisant au maximum les fonctionnalités Javascript. 

Après plusieurs recherches et des heures de réfléxion, j'ai choisi dans un premier temps le jeu du pendu. Le but du jeu est de deviner un mot. A chaque proposition de lettre figurant dans le mot à deviner, le joueur continue la partie. Si la lettre ne fait pas partie du mot alors le joueur gagnera un morceau de la potence sur laquelle il sera pendu s'il ne trouve pas le mot à deviner. 

Ce jeu ne me sortait pas trop de ma zone de confort. J'ai donc choisi de mélanger deux jeux : le principe du pendu (deviner un mot) et cache-cache (trouver quelque chose qui est caché).

Le principe de mon jeu est de deviner un mot grâce à un indice dans un premier temps et d'une image cachée dans un second temps. 

Dès lors que le joueur sélectionne une lettre qui fait partie du mot à deviner alors un pan d'un rideau noir disparait laissant apparaitre une image. 

Si le joueur propose une lettre non comprise dans le mot à deviner alors la lettre deviendra rouge. Si elle fait partie du mot à deviner, elle deviendra grise. 

Il n'y a pas de niveau dans mon jeu. Ni de chrono. 



Pour ce faire, j'ai élaboré des maquettes fonctionnelles dont voici les liens :

https://wireframe.cc/pro/pp/13bdd4dea265241 

<strong>User stories : </strong>

- en tant que prof je souhaite savoir si les simploniennes et simploniens ont bien appliqués les consignes en vérifiant leurs maquettes et codes (et leur jeu). 
- en tant que joueur, je souhaite m'amuser lors d'une partie et passer du bon temps. 
- en tant que recruteur, je souhaite voir les projets d'un candidat et vérifier les technos utilisés lors de ces projets étudiants. 
- en tant qu'étudiant, je souhaite jouer aux jeux de mes collègues et avoir des idées des promos précédentes.





En ce qui concerne les technos utilisées :


  

\- en HTML, pour le placement des blocs j’ai utilisé du Boostrap et Flexbox, ce qui m’a permis d’être un peu plus à l’aise avec ces outils.

J’ai choisi de mettre les lettres de l’alphabet dans un tableau avec une classe pour pouvoir sélectionner mes lettres par la suite en CSS ou JS.  


  

\- en CSS, c’est quelque chose d’assez classique. J’ai utilisé des propriétés que je ne connaissais pas (background size, repeat & position pour mieux positionner mes images sous mon blackscreen).


  

\- en JS, mon code est entièrement commenté étape par étape pour 2 raisons. D’abord, pour que ce soit clair pour moi et d’autres éventuelles personnes qui souhaitent lire mon code. Ensuite, j’ai trouvé cela plus facile pour avancer. Je suivais d’un côté mon board Gitlab, je commentais en français et je codais en JS. Ces étapes peuvent paraître plus longues mais je suivais ma logique et c’est ce qui m’a permis de finir dans les temps et de comprendre mon projet. Évidemment, j’ai supprimé certaines lignes de commentaires afin de clarifier mon code.

En cours de projet, j’ai décidé d’utiliser des technos vues en cours comme les classes. Il y a donc une classe. Cette classe a simplifié mon code car elle m’a permis de déclarer une variable qui contenait un tableau avec tous les paramètres dont j’avais besoin au lieu d’avoir 3 tableaux et 3 variables.  


  


  

<strong> 2) Diagrammes : </strong>



Diagramme de ma classe Guess :  





![classGuess](./images/classGuess.png)



<strong> 3) Organisation du projet : </strong>



Pour ce qui est de l’organisation et de l’utilisation des sprints, j’ai procédé par étape.  

J’ai fait 5 milestones dans lesquelles j’ai assigné des issues avec des objectifs à réaliser par semaine. Au fur et à mesure, sur mon board, je faisais passer les issues de ‘to do’ à ‘doing’ et enfin à ‘closed’ quand la tâche était terminée.  

Cette façon de procéder m’a tellement plu que je pense l’utiliser pour les prochains projets.  


  

<strong> 4) Maquette : </strong>



Les maquettes ont été faites avant quelques modifications. La version présentée dans la maquette ne correspond pas tout à fait à la version en ligne.  

TOUTEFOIS, ces différences donneront lieu à une version 2 du jeu où tous les petits bugs et incohérences seront corrigés tout comme les mots probablement.  





![Etape1](./images/Etape1.png)



![Etape2](./images/Etape2.png)



![Etape3](./images/Etape3.png)



![Etape4](./images/Etape4.png)





![Etape5](./images/Etape5.png)



