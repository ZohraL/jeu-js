let blackScreen = document.querySelector("#black-screen");
let nbLettreFound = 0;

/* ici j'ai déclaré des variables avec des arrays 
let words = ["LAPTOP", "MOBILEDEVICE", "PIXEL", "HARDWARE", "VORTEX", "SCREEN", "BUG", "SPAM", "LOOP", "ITERATION"];
let hints = ["Keyboard", "Small Screen", "Display quality", "Computer", "Whirlwind", "Motion picture display", "Failure", "Unwanted mail", "Spiral", "Repetition"];

Ci-dessous, une variable avec une instance de ma classe Guess. La classe va venir simplifier mon code. 
En effet, au lieu d'avoir 3 tableaux et 3 variables, 
je n'ai plus qu'une variable qui contient tous les paramètres de ma classe. */

let word = [
    new Guess("LAPTOP", "Keyboard", 'laptop.png'),
    new Guess("MOBILE DEVICE", "Small Screen", "mobiledevice.png"),
    new Guess("PIXEL", "Display quality", "pixel.png"),
    new Guess("HARDWARE", "Computer", "hardware.jpg"),
    new Guess("VORTEX", "Whirlwind", "vortex.jpg"),
    new Guess("SCREEN", "Motion picture display", "screen.png"),
    new Guess("BUG", "Failure", "bug.png"),
    new Guess("SPAM", "Unwanted mail", "spam.jpg"),
    new Guess("LOOP", "Spiral", "loop.png"),
    new Guess("ITERATION", "Repetition", "iteration.png"),
]



let choice = Math.floor(Math.random() * word.length);
// ci-dessus, une variable qui comprend une fonction pour choisir aléatoirement.
let term = word[choice];
console.log(term);
// ci-dessus, un mot est choisi aléatoirement.
let indice = document.querySelector("#hint");
indice.innerHTML = `<strong>` + term.hints + `</strong>`;
// ci-dessus, un indice s'affiche dans l'encadré prévu à cet effet.


// la fonction ci-dessous permet de créer l'élément "curtain" dans l'HTML. J'applique des propriétés CSS à mon élément HTML.
function createCurtain() {
    let curtain = document.createElement('p');
    curtain.style.backgroundColor = 'black';
    curtain.style.width = 200 / term.words.length + "px"; // ici, la largeur des curtains changera en fonction de la taille des mots.
    blackScreen.appendChild(curtain);
    curtain.style.height = "215px";
    blackScreen.style.backgroundImage = "URL('./images/" + term.images; "')"
}

for (let index = 0; index < term.words.length; index++) {
    createCurtain();
}

let letters = document.querySelectorAll('.click');
for (let letter of letters) {
    letter.addEventListener('click', function () {
        if (term.words.includes(letter.textContent)) {
            //la case devient grise si la lettre trouvée fait partie du mot...
            letter.style.backgroundColor = 'grey';
        } else {
            // ... et rouge si elle ne fait pas partie du mot.
            letter.style.backgroundColor = 'red';
        }
        //je sélectionne l'élément HTML ID "response" et je déclare la variable "response".
        let response = document.querySelector('#response');
        console.log(letter.textContent);
        // les lettres proposées par le jouer font parties du mot, ce qui revient à 
        // response.textContent = letter.textContent;
        response.textContent += letter.textContent;
        // je sélectionne tous les éléments HTML paragraphe et je déclare la variable "curtains".
        let curtains = document.querySelectorAll('p');
        // je fais une condition qui dit que si le mot à deviner contient des lettres proposées par le joueur...
        if (term.words.includes(letter.textContent)) {
            // ... alors un pan des rideaux va disparaitre.
            curtains[nbLettreFound].style.display = 'none';
            nbLettreFound++;
        }
    })
}
// je sélectionne l'élément HTML ID "proposition" et je déclare la variable "proposition".
let proposition = document.querySelector('#proposition');
// je sélectionne l'élément HTML ID "submit" et je déclare la variable "submit".
let submit = document.querySelector('#submit');
// je fais qu'au click du bouton submit il se passe un événement ...
submit.addEventListener('click', function () {
    // ... cette événement est une condition, qui vient vérifier si le mot à deviner correspond en tout point à la proposition du joueur.
    if (term.words === proposition.value.toUpperCase()) { //la fonction toUpperCase fait passer comme ok les propositions faites en minuscules.
        // si oui alors une fenêtre affichera "GG".
        alert('GG');
    } else {
        // si non alors on dévoile le mot qui était attendu. 
        alert('Too bad! The word was ' + term.words);
    }
})
